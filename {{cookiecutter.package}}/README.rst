************************
{{cookiecutter.package}}
************************

.. image:: https://badge.fury.io/py/{{cookiecutter.package}}.svg
   :target: https://badge.fury.io/py/{{cookiecutter.package}}
   :alt: PyPI package

.. image:: https://img.shields.io/pypi/dm/{{cookiecutter.package}}.svg
   :target: https://badge.fury.io/py/{{cookiecutter.package}}
   :alt: PyPi package downloads

.. image:: https://img.shields.io/badge/license-GPL-brightgreen.svg
   :target: LICENSE
   :alt: Repository license

.. image:: https://readthedocs.org/projects/{{cookiecutter.package}}/badge/?version=latest
   :target: https://{{cookiecutter.package}}.readthedocs.io/en/latest/
   :alt: Documentation status

.. image:: {{cookiecutter.git_hosting_url}}/badges/master/pipeline.svg
   :target: {{cookiecutter.git_hosting_url}}/commits/master
   :alt: CI pipeline status

.. image:: {{cookiecutter.git_hosting_url}}/badges/master/coverage.svg
   :target: {{cookiecutter.git_hosting_url}}/commits/master
   :alt: Coverage report

.. image:: https://img.shields.io/badge/support-maintainers-brightgreen.svg
   :target: {{cookiecutter.support}}
   :alt: Support badge

{{cookiecutter.package_description}}
------------------------------------

.. _documentation:

Documentation
*************

    https://{{cookiecutter.package}}.readthedocs.io/

Code of Conduct
***************

By participating in the {{cookiecutter.package}} community, you agree on the
following code of conduct. Please see `CODE_OF_CONDUCT`_ for the full text.

.. _CODE_OF_CONDUCT: ./CODE_OF_CONDUCT
