************
Installation
************

.. code-block:: bash

    $ python3 -m venv .venv 
    $ source .venv/bin/activate
    $ pip install {{cookiecutter.package}}

.. note::

    This tool only supports Python >= 3.5.

.. note::

    It is recommended to use a `virtual environment`_ when
    installing Python packages. You should also consider upgrading
    your `setuptools`_ installation with ``pip install -U
    setuptools``.

    .. _virtual environment: https://docs.python.org/3/tutorial/venv.html
    .. _setuptools: http://setuptools.readthedocs.io/
