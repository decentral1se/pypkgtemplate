author = '{{cookiecutter.author}}'
copyright = '2019, {{cookiecutter.author}}'
html_static_path = ['_static']
html_theme = 'alabaster'
master_doc = 'index'
project = '{{cookiecutter.package}}'
templates_path = ['_templates']
