---
name: ✨ Feature request
about: Suggest an idea for the project
---
<!--- Verify first that your issue is not already reported on the issue tracker -->
<!--
  If you do not have capacity to contribute to resolving the that you will now
  open, please consider supporting the maintainer and donating at
  {{cookiecutter.support}}.
-->

# Issue Type

- Feature request

# {{cookiecutter.package}} details

```
{{cookiecutter.package}} --version
```

# Issue Type

- Feature request

# {{cookiecutter.package}} details

```
{{cookiecutter.package}} --version
```

{{cookiecutter.package}} installation method (one of):

- source
- pip

# Desired Behavior

Please give some details of the feature being requested or what should happen
if providing a bug report.
