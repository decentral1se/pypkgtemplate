<!--- Verify first that your issue is not already reported on the issue tracker -->
<!--
  If you do not have capacity to contribute to resolving the that you will now
  open, please consider supporting the maintainer and donating at
  https://lukewm.info/support/
-->

# Issue Type

- Feature request

# Desired Behavior

Please give some details of the feature being requested or what should happen
if providing a bug report.
